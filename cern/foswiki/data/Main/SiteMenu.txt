%META:TOPICINFO{author="pete" comment="" date="1608031369" format="1.1" reprev="2" version="2"}%
%META:TOPICPARENT{name="NatSkin"}%
---+ %TOPIC%

Default %TOPIC% component. Please do __not__ modify. Use [[%USERSWEB%.%TOPIC%]] instead.

---++ Implementation
<verbatim class="tml">
%STARTINCLUDE%<ul>
  <li>[[%USERSWEB%.%HOMETOPIC%][%MAKETEXT{"Home"}%]]</li>
  <!-- li>[[News.%HOMETOPIC%]]</li -->
  <!-- li>[[Knowledge.%HOMETOPIC%]]</li -->
  <!-- li>[[Projects.%HOMETOPIC%]]</li -->
%IF{"'%USERNAME%' ingroup 'AdminGroup'" 
  then="$percntINCLUDE{\"%SYSTEMWEB%.SiteMenu\" section=\"adminmenu\"}$percnt"
}% %INCLUDE{"%USERSWEB%.SiteMenu" section="helpmenu"}%
</ul>%STOPINCLUDE% 
</verbatim>

---++ Subwebs
<verbatim class="tml">
%STARTSECTION{"subwebs"}%%FLEXWEBLIST{
  include="%URLPARAM{"theweb" default="%theweb{default=""}%"}%.*"
  exclude="%URLPARAM{"theweb" default="%theweb%"}%|.*(%URLPARAM{"excludeweb" default="does not exist"}%).*"
  exclude="%URLPARAM{"theweb" default="%theweb{default=""}%"}%|.*(%URLPARAM{"excludeweb" default="%exclude{default="does not exist"}%"}%).*"
  header="<li>"
  subheader="<ul><li>"
  format="[[$web.%HOMETOPIC%][%JQICON{"%URLPARAM{"icon" default="%icon{default="bullet_white"}%"}%"}% $title]]"
  separator="</li><li>"
  subfooter="</li></ul>"  
  footer="</li>"
}%%ENDSECTION{"subwebs"}%
</verbatim>

---++ Help menu
<verbatim class="tml">
%STARTSECTION{"helpmenu"}%<li>[[%SYSTEMWEB%.WebHome][%MAKETEXT{"Help"}%]]
  <ul>
    <li>[[%SYSTEMWEB%.WelcomeGuest][%TMPL:P{"HOME_ICON"}% %MAKETEXT{"Welcome"}%]]</li>
    <li>[[%SYSTEMWEB%.UserDocumentationCategory][%TMPL:P{"DOCU_ICON"}% %MAKETEXT{"User docs"}%]]</li>
    <li>[[%SYSTEMWEB%.AdminDocumentationCategory][%TMPL:P{"DOCU_ICON"}% %MAKETEXT{"Admin docs"}%]]</li>
    <li>[[%SYSTEMWEB%.DeveloperDocumentationCategory][%TMPL:P{"DOCU_ICON"}% %MAKETEXT{"Developer docs"}%]]</li>
    <li>[[%SYSTEMWEB%.ReferenceManual][%TMPL:P{"MANUAL_ICON"}% %MAKETEXT{"Reference manual"}%]]</li>
    <li>[[%SYSTEMWEB%.InstalledPlugins][%TMPL:P{"PLUGIN_ICON"}% %MAKETEXT{"Installed plugins"}%]]</li>
    <li><hr /></li>
    <li>[[Sandbox.%HOMETOPIC%][%TMPL:P{"SANDBOX_ICON"}% %MAKETEXT{"Sandbox"}%]]</li>%IF{"context passwords_modifyable" then="
    <li>[[%SYSTEMWEB%.ResetPassword][%TMPL:P{"PASSWORD_ICON"}% %MAKETEXT{"Password forgotten"}%]]</li>"}%
    <li><hr /></li>
    <li>[[https://mattermost.web.cern.ch/it-dep/channels/wiki-applications][%TMPL:P{"HELP_ICON"}% %MAKETEXT{"Mattermost"}%]]</li>
    <li>[[https://discourse.web.cern.ch/c/collaborative-editing/wikis/12][%TMPL:P{"QUESTION_ICON"}% %MAKETEXT{"Discourse"}%]]</li>
    <li>[[https://cern.service-now.com/service-portal/search.do?q=twiki][%TMPL:P{"BUG_ICON"}% %MAKETEXT{"Service Now"}%]]</li>
    <li>[[https://foswiki.org/Support/WebHome][%TMPL:P{"ONLINE_HELP_ICON"}% %MAKETEXT{"Foswiki.org"}%]]</li>
  </ul>
</li>%ENDSECTION{"helpmenu"}%
</verbatim>

---++ Admin menu
<verbatim class="tml">
%STARTSECTION{"adminmenu"}%<noautolink>
<li><a>%MAKETEXT{"Admin"}%</a>
  <ul>
    %IF{"isweb 'Applications'"
       then="<li>
         [[Applications.%HOMETOPIC%][%TMPL:P{"FOLDER_OPEN_ICON"}% %MAKETEXT{"Applications"}%]]
         <ul class=\"ajaxMenu {url:'%SCRIPTURL{"view"}%/%SYSTEMWEB%/SiteMenu?skin=text;section=subwebs;theweb=Applications;icon=fa-folder-o%IF{"defined EXCLUDEWIKIAPPS" then=";excludeweb=%EXCLUDEWIKIAPPS%"}%'}\"></ul>
       </li>"
    }%<!-- -->
    <li>[[%TRASHWEB%.%HOMETOPIC%][%TMPL:P{"TRASH_ICON"}% %MAKETEXT{"Trash"}%]]</li>
    <li>[[%BASEWEB%.%WEBPREFSTOPIC%][%TMPL:P{"SETTINGS_ICON"}% %MAKETEXT{"Web preferences"}%]]</li>
    <li>[[%LOCALSITEPREFS%][%TMPL:P{"SETTINGS_ICON"}% %MAKETEXT{"Site preferences"}%]]</li>
    <li>[[%SYSTEMWEB%.SitePermissions][%TMPL:P{"PASSWORD_ICON"}% %MAKETEXT{"Site permissions"}%]]</li>
    <li>[[%SCRIPTURLPATH{"configure"}%][%TMPL:P{"CONFIGURE_ICON"}% %MAKETEXT{"Configure Foswiki"}%]]</li>
    <li> <hr /> </li>
    <li>[[%SYSTEMWEB%.WebCreateNewWeb][%JQICON{"fa-plus"}% %MAKETEXT{"Create a new web"}%]]</li>
    <li>[[%SCRIPTURL{"rename"}%/%BASEWEB%/%BASETOPIC%?action=renameweb][%JQICON{"fa-arrows"}% %MAKETEXT{"Rename current web"}%]]</li>
  </ul>
</li>
</noautolink>%ENDSECTION{"adminmenu"}%
</verbatim>

%META:PREFERENCE{name="ALLOWTOPICVIEW" title="ALLOWTOPICVIEW" type="Set" value="*"}%
%META:PREFERENCE{name="PERMSET_VIEW" title="PERMSET_VIEW" type="Local" value="everybody"}%
