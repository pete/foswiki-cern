%META:TOPICINFO{author="BaseUserMapping_333" comment="" date="1604572080" format="1.1" reprev="2" version="2"}%
%META:TOPICPARENT{name="WebHome"}%
%INCLUDE{"%IF{"context can_login" then="%SYSTEMWEB%.WebLeftBarLogin" else="<nop>"}%" warn="off"}%

%STARTSECTION{"topics"}%
   * *%MAKETEXT{"Toolbox"}%*
   * [[%SCRIPTURLPATH{"view"}%/%BASEWEB%/WebCreateNewTopic?topicparent=%BASETOPIC%][ %ICON{"newtopic"}% %MAKETEXT{"Create New Topic"}%]]
   * [[WebNotify][ %ICON{"notify"}% %MAKETEXT{"Notifications"}%]]
   * [[WebRss][ %ICON{"feed"}% %MAKETEXT{"RSS Feed"}%]] %IF{"istopic '%STATISTICSTOPIC%'"  then="
   * [[$percntSTATISTICSTOPIC$percnt][ $percntICON{\"statistics\"}$percnt $percntMAKETEXT{\"Statistics\"}$percnt]]"}%
   * [[WebPreferences][ %ICON{"wrench"}% %MAKETEXT{"Preferences"}%]] 
%ENDSECTION{"topics"}%
---
%STARTSECTION{"nav"}%
   * *%MAKETEXT{"Navigation"}%*
   * [[WebTopicList][ %ICON{"index"}% %MAKETEXT{"Index"}%]]
   * [[%BASEWEB%.WebSearch][ %ICON{"searchtopic"}% %MAKETEXT{"Search"}%]]
   * [[WebChanges][ %ICON{"changes"}% %MAKETEXT{"Changes"}%]]
   * [[SiteMap][ %ICON{"sitemap"}% %MAKETEXT{"Site Map"}%]]
   * [[ChildTopics][ %ICON{"index"}% %MAKETEXT{"Child topics"}%]]
   * [[MyTopics][ %ICON{"indexlist"}% %MAKETEXT{"My topics"}%]]
%ENDSECTION{"nav"}%
---
%STARTSECTION{"support"}%
   * *%MAKETEXT{"Support"}%*
   * [[System/UserDocumentationCategory][ %ICON{"help"}% %MAKETEXT{"User Docs"}%]]
   * [[https://mattermost.web.cern.ch/it-dep/channels/wiki-applications][ %ICON{"persons"}% %MAKETEXT{"Mattermost"}%]]
   * [[https://discourse.web.cern.ch/c/collaborative-editing/wikis/12][ %ICON{"persons"}% %MAKETEXT{"Discourse"}%]]
   * [[https://foswiki.org/Support/WebHome][ %ICON{"foswiki"}% %MAKETEXT{"Foswiki Support"}%]]
   * [[https://cern.service-now.com/service-portal?id=service_element&name=twiki][ %ICON{"help"}% %MAKETEXT{"Service Now"}%]]
%ENDSECTION{"support"}%


